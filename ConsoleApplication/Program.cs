﻿using System;
using System.Collections.Generic;
using System.Linq;
using AgilityTracker.BLL;
using AgilityTracker.BLL.Factory;
using AgilityTracker.BLL.DataModel;
using AgilityTracker.BLL.Repository;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnitOfWorkFactory factory = new AgilityTrackerUnitOfWorkFactory();
            IUnitOfWork agilityUnitOfWork = factory.CreateInstance();
            var userRepository = agilityUnitOfWork.GetRepository<ProjectRepository>();

            userRepository.Add(new Project() { Title = "qwe", Description = "qweqweqweqwe" });
            agilityUnitOfWork.Commit();

            //Console.ReadKey();
            agilityUnitOfWork.Dispose();
        }
    }
}
