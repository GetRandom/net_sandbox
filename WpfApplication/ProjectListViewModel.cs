﻿using AgilityTracker.BLL.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication
{
    public class ProjectListViewModel : INotifyPropertyChanged
    {
        protected ObservableCollection<ProjectViewModel> projects;

        public ObservableCollection<ProjectViewModel> Projects
        {
            get { return this.projects; }
            set
            {
                this.projects = value;
                this.PropertyChanged(this, new PropertyChangedEventArgs("Projects"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ProjectListViewModel(IEnumerable<Project> projects)
        {
            this.projects = new ObservableCollection<ProjectViewModel>(projects.Select(p => new ProjectViewModel(p)));
        }
    }
}
