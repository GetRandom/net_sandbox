﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AgilityTracker.BLL.DataModel;

namespace WpfApplication
{
    public class ProjectViewModel : INotifyPropertyChanged
    {
        protected Project project;

        public int ID
        {
            get { return this.project.ID; }
            set
            {
                this.project.ID = value;
                this.PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
        }

        public string Title
        {
            get { return this.project.Title; }
            set
            {
                this.project.Title = value;
                this.PropertyChanged(this, new PropertyChangedEventArgs("Title"));
            }
        }

        public string Description
        {
            get { return this.project.Description; }
            set
            {
                this.project.Description = value;
                this.PropertyChanged(this, new PropertyChangedEventArgs("Description"));
            }
        }

        public ProjectViewModel(Project project)
        {
            this.project = project;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
